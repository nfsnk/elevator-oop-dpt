package ElevatorPackage;

public interface ElevatorInterface {
    public static final int Topfloor = 20;
    public static final int GroundFloor = 0;

    /*
    The following method is used to ask the user where she/he wishes to go
     */
    public void whereToGo();

    /*
    The following method allows the elevator to go up to the destination floor
    @param destination
     */
    public void goUp(int destination);


    /*
    The following method allows the elevator to go down to the destination floor
    @param destination
     */
    public void goDown(int destination);
}
