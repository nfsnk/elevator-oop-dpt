package ElevatorPackage;

import java.util.Scanner;

/*
This class is the representation of the software used for one elevator.
It utilizes the public interface @ElevatorInterface already defined and implements the methods.
 */
public class Elevator implements ElevatorInterface{
    public int currentFloor = 0;

    public Elevator() {
        super();
    }

    @Override
    public void whereToGo() {
        System.out.println("You are currently on the floor "+currentFloor);
        System.out.println("Please where do you want to go?");
        try(Scanner sc = new Scanner(System.in)) {
            int destination = sc.nextInt();
            if (destination > Topfloor || destination < GroundFloor){
                System.out.println("The floor you want to go to is unreachable;\nPlease enter a new destination floor between "+GroundFloor+" and "+Topfloor);
                whereToGo();
            }
            else if (destination == currentFloor) {
                System.out.println("You are already on the floor " +currentFloor);
                whereToGo();
            }
            else{
                System.out.println("Door closing...\nPlease stand clear of the closing door.");
                if(destination > currentFloor) { goUp(destination);}
                else if (destination < currentFloor) { goDown(destination);}
                System.out.println("Door opening...\n\n");

                whereToGo();

                }
            } catch (Exception e){e.printStackTrace();}

        }


    @Override
    public void goUp(int destination) {
        while (destination -1 > currentFloor ++){
            System.out.println("Going up... \nCurrent Floor: "+currentFloor+ " --------- Destination Floor: "+destination+"\n");
        }
        System.out.println("Going up... \nCurrent Floor: "+currentFloor);
    }

    @Override
    public void goDown(int destination) {
        while (destination +1 < currentFloor --){
            System.out.println("Going down... \nCurrent Floor: "+currentFloor+ " --------- Destination Floor: "+destination+"\n");
        }
        System.out.println("Going down... \nCurrent Floor: "+currentFloor);
    }


}

